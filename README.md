# PocketWorldExample

PocketWorldExample is a project sample that showcases some of the PocketWorld capabilities.

## Basics: How to use
- Open the project in the default level (ThirdPersonMap)
- Hit Play
- Press the "E" key to open/close the example menu


## What is Pocket Worlds?
Pocket Worlds is one of the multiple awesome plugins you can find in [Lyra Starter Game](https://dev.epicgames.com/community/learning/paths/Z4/lyra-starter-game). It allows for easy streaming of levels. As I understand it, its designed as a cleaner, compact solution for the classic way to render 3D character in menus, which usually implies loading a map outside the normal gameplay boundaries.

## Credits and warnings

- All files marked with epic's copyright are the copyright of Epic Games, and not subject to the MIT License for the rest of the repository.
- **I'M NOT THE AUTHOR OF POCKET WORLDS**. The creator of the plugin is **Nick Darnell**, and all credits and rights for it belong to Epic Games.
- I did the initial commit for this project, but then **[Ryan DowlingSoka](https://gitlab.com/Ryan-DowlingSoka)** made a lot of improvements and cleanups, including fixing core issues I had with the MenuCharacterRender material. More information on that [here](https://twitter.com/RyanDowlingSoka/status/1570811838446837762).
- Lastly, shotouts to **Santi Alex Mañas**, as I worked with him in the first version of this implementation for the project Tribal Robots

## Most important classes and assets
### C++
- **Pocket worlds plugin**: I would recomend starting by reading the whole Pocket Worlds plugin, as it's quite small and verbose.
- **PWEPocketStage**: Base class for a "Stage" actor, which is used as the target for a pocket capture. This exposes Pre and Post blueprint implementable events around each render capture.
- **PWEUISubsystem**: A Game Instance Subsystem I created to handle the logic for spawning the Pocket World, and to store the Pocket Capture & Pocket Stage made in the PocketLevel.

### Blueprint
- **BP_ThirdPersonCharacter**: for this example, I streamed the PocketLevel in the BeginPlay of the default character, and handled the input for the test menu here too.
- **BP_MenuCharacter**: this is the character we will render in the menu. In this sample I just used a mesh for the rendering process, but in a real project this should work as a listener that can alter their aspect, animations and other parts of its visual aspect depending on the gameplay states (like equipping armor or reproducing special idles). I'd expect the Stage to be subclassed and handle the spawning and controlling of the Pawn's behaviors and appearance.
- **BP_MenuStage**: this is the stage which is the target for the character, it contains a camera, and logic to handle the alpha masked phase, and the logic to spawn the character as a visible item.
### Widgets
- **WBP_MenuCharacterRender**: this widget handles the rendering of the character in the pocket world. Is a very simple version that has no easy preview on the result (which would be a really nice improvement for UI implementation).
- **WBP_TestMenu**: simple mockup I put together to make the demo fancier :D

### Others
- **DA_MenuCharacter**: the Pocket Level DataAsset that must be present with each PocketWorld
- **M_MenuCharacterRender** & **MI_MenuCharacterRender**: the instance and material that displays what the camera captures in the PocketWorld, this is based off of the included Material'/PocketWorlds/M_PocketCaptureMasked.M_PocketCaptureMasked' but alpha composite for future improvements. (Particle effects, glows, etc...)
- **M_SkyLighting**: fake skylighting material applied to a translucent card and attached to the screen. Manually hidden in the Alpha mask pass.
- **P_MenuCharacter**: the level we are going to stream. It contains a very basic lighting to make our character actually visible, it also contains the BP_MenuStage, and a HiddenInGame copy of the character for previewing lighting, since the BP_MenuStage spawns the character at runtime. It also contains a inverted sphere black backdrop to hide any background visible scenes.

### To do
- Add better lighting to the pocket world level. Skylights cause issues with pocket worlds, but are really important for these scenes.
- Lighting channels could be used to give the pocket world directional lights.
- Include a way to generate a preview for the character render, so its easier to operate with it in UMG (using some tips 
Ryan DowlingSoka provided)

